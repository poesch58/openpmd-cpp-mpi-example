#include <mpi.h>
#include <openPMD/auxiliary/Environment.hpp>
#include <openPMD/openPMD.hpp>

#include <cstdint>
#include <numeric>

int main()
{
    int mpi_size, mpi_rank;
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Init(nullptr, nullptr);
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);

    using namespace openPMD;

    using extent_t = Extent::value_type;

    /*
     * Define the size of the simulation that we are pretending to run.
     */
    Extent gridSize{extent_t(mpi_size), 16, 16};
    Offset localBlockStart{extent_t(mpi_rank), 0, 0};
    Extent localBlockExtent{1, 16, 16};
    extent_t localBlockFlatSize = std::accumulate(
        localBlockExtent.begin(),
        localBlockExtent.end(),
        1,
        [](auto acc, auto val) { return acc * val; });
    constexpr extent_t numberOfParticlesPerRank = 100;

    std::string filenameExtension = auxiliary::getEnvString("EXT", "bp");

    /*
     * Root level of openPMD.
     */
    Series series(
        "simData." + filenameExtension, Access::CREATE, comm, "@cfg.toml");

    for (size_t currentIterationIndex = 0; currentIterationIndex < 100;
         currentIterationIndex += 10)
    {
        // main simulation loop
        // IO every ten steps

        using field_datatype = float;
        // writeIterations() is (currently) a restricted API intended
        // to ensure that the I/O workflow adheres to streaming requirements.
        // For random-access use `series.iterations` (not good for ADIOS2).
        Iteration currentIteration =
            series.writeIterations()[currentIterationIndex];

        currentIteration.setTime(double(currentIterationIndex));
        currentIteration.setTimeUnitSI(1.0);

        /** @todo Can we define further attributes.
         */

        // Write field. Here: An electrical field.

        auto E_field = currentIteration.meshes["E"];
        E_field.setAxisLabels({"x", "y", "z"});
        E_field.setGridGlobalOffset({0., 0., 0.});
        E_field.setGridSpacing<double>({1., 1., 1.});
        E_field.setGridUnitSI(1.0);
        {
            using U = UnitDimension;
            E_field.setUnitDimension(
                {{U::L, 1}, {U::M, 1}, {U::T, -3}, {U::I, -1}});
        }

        for (auto component : {"x", "y", "z"})
        {
            std::unique_ptr<field_datatype[]> sampleData{
                new field_datatype[localBlockFlatSize]};
            std::iota(
                sampleData.get(), sampleData.get() + localBlockFlatSize, 0);

            auto fieldComponent = E_field[component];
            fieldComponent.setUnitSI(1.0);
            fieldComponent.setPosition<float>({0.5, 0.5, 0.5});

            /** @todo Write field data
             */
        }

        // Write particle data. Here: electrons.

        ParticleSpecies electrons = currentIteration.particles["e"];
        for (auto component : {"x", "y", "z"})
        {
            using position_datatype = double;
            auto position = electrons["position"][component];
            auto positionOffset = electrons["positionOffset"][component];

            position.setUnitSI(1.0);
            positionOffset.setUnitSI(1.0);

            std::vector<position_datatype> sampleData(numberOfParticlesPerRank);
            std::iota(sampleData.begin(), sampleData.end(), 0);

            /** @todo Write electron data
             */
        }
        // Close the current time step.
        // In streaming-like backends, readers can now see the data.
        currentIteration.close();
    }

    series.close();
    MPI_Finalize();
}
